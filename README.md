# Data Split

A script for splitting datasets into train/validation/test sets etc.

# Install

The script is native Python. Fetch the script and run with `python data-split.py`.
