#!/bin/python3
import argparse
import os
import random
import re
import sys
import warnings
from glob import glob
from pprint import pprint


def create_splits(files, splits_fractions, verbose):
    files = list(files)
    random.shuffle(files)
    numbers = {k: int(v * len(files)) for k, v in splits_fractions.items()}

    # Detect and naively assign any missing numbers
    missing = len(files) - sum(numbers.values())
    if missing > 0:
        chosen = random.choices(
            list(splits_fractions.keys()), weights=list(splits_fractions.values()), k=1
        )[0]
        numbers[chosen] += missing

    if verbose:
        print("Splits:", numbers)

    prev = 0
    collected = {}
    for k, v in numbers.items():
        collected[k] = files[prev : v + prev]
        prev += v

    return collected


def directories(arg):
    dirs = arg.split(":")
    existing = [d for d in dirs if os.path.exists(d)]
    if existing:
        print(
            "Director{y,ies} '%s' exist(s). Please remove it/them before continuing"
            % "', '".join(existing),
            file=sys.stderr,
        )
        sys.exit(1)
    return dirs


def floats(arg):
    fractions = tuple(map(float, arg.split(":")))
    if sum(fractions) != 1:
        print("Split fractions must add to 1", file=sys.stderr)
        sys.exit(1)
    return fractions


def regex_substitution(arg):
    pattern, sub = arg.split(":")
    regex = re.compile(pattern)

    def _closure(path):
        new, n = regex.subn(sub, path)
        if n == 0:
            warnings.warn("Pattern '%s' failed to match on file '%s'" % (pattern, path))
            return None
        if not os.path.isfile(new):
            print("Error associated file '%s' not found" % new, file=sys.stderr)
            sys.exit(1)
        return new

    return _closure


def _create_link(source, dest_dir):
    commonpath = os.path.commonpath(
        [os.path.abspath(dest_dir), os.path.abspath(source)]
    )
    dest = os.path.join(commonpath, dest_dir, os.path.relpath(source, commonpath))
    linkpath = os.path.relpath(source, os.path.dirname(dest))
    os.makedirs(os.path.dirname(dest), exist_ok=True)
    os.symlink(linkpath, dest)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "inputs", help="All input files to create dataset splits for", nargs="+"
    )
    parser.add_argument(
        "--dirs",
        type=directories,
        default="train:validation:test",
        help="Name of directories to save dataset splits in",
    )
    parser.add_argument(
        "--fractions",
        type=floats,
        default="0.6:0.2:0.2",
        help="Split fractions corresponding to --dirs",
    )
    parser.add_argument(
        "--associated-file",
        action="append",
        default=[],
        type=regex_substitution,
        help="A substitution regex which when applied to an `input` produces the "
        "path to an associated file which will follow with the original `input` file. "
        "Example: '(.+/)?images/(.+)jpg:\\1labels/\\2.txt'",
    )
    parser.add_argument(
        "--link-name",
        choices=["path", "hash"],
        required=True,
        help="Name of the output links. `path` mimics the directory structure and "
        "file names of the inputs. `hashes` names links according to the hashes "
        "of the inputs (NB associated files will get named according to the associated "
        "input file).",
    )
    parser.add_argument(
        "--dry-run", action="store_true", help="Only print out the resulting splits"
    )
    parser.add_argument("--verbose", action="store_true", help="Increase verbosity")
    args = parser.parse_args()

    splits_fractions = dict(zip(args.dirs, args.fractions))
    missing = tuple(filter(lambda x: not os.path.isfile(x), args.inputs))
    if missing:
        print("No such file(s):\n", "\n".join(missing), file=sys.stderr)
        exit(1)

    splits = create_splits(args.inputs, splits_fractions, args.verbose)

    for name, paths in tuple(splits.items()):
        for path in tuple(paths):
            for associated_file in args.associated_file:
                new = associated_file(path)
                if new is not None:
                    splits[name].append(new)

    if args.verbose or args.dry_run:
        pprint(splits)

    if args.dry_run:
        sys.exit(0)

    if args.link_name == "path":
        for name, paths in splits.items():
            tuple(map(lambda path: _create_link(path, name), paths))
    elif args.link_name == "hash":
        raise NotImplementedError


if __name__ == "__main__":
    main()
